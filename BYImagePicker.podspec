Pod::Spec.new do |s|
  s.name             = 'BYImagePicker'
  s.version          = '0.5.0'
  s.summary          = 'A short description of BYImagePicker.'
  s.description      = 'A long description of BYImagePicker'
  s.homepage         = 'https://Elmund0@bitbucket.org/barisaliyilmaz/byimagepicker.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Baris Yilmaz' => 'mail@barisyilmaz.me' }
  s.source           = { :git => 'https://Elmund0@bitbucket.org/barisaliyilmaz/byimagepicker.git', :tag => s.version.to_s }
  s.ios.deployment_target = '9.0'
	s.frameworks = 'Foundation', 'UIKit'
  s.source_files = 'BYImagePicker/**/*.{swift}'
  
  #s.resource_bundles = {
  #	'BYImagePicker' => ['BYImagePicker/Assets/BYImagePicker.bundle']
  #}
	
	s.resources = ['BYImagePicker/Assets/*.xib', 'BYImagePicker/Assets/BYImagePicker.bundle']

  # s.dependency 'AFNetworking', '~> 2.3'
end


