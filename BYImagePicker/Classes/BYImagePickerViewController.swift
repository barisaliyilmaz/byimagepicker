//
//  BYImagePickerViewController.swift
//  BYImagePicker
//
//  Created by Baris Yilmaz on 08/12/2016.
//  Copyright © 2016 Baris Yilmaz. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import AVKit

public protocol BYImagePickerViewDelegate {
    func imagePicker(imagePicker: BYImagePickerViewController, withImage image:UIImage, withCaption text:String?)
    func imagePicker(imagePicker: BYImagePickerViewController, withVideoData data:Data, withThumbnail image:UIImage, withCaption text:String?)
}

open class BYImagePickerViewController: UIViewController {
    // MARK: ******************************************
    // MARK: ************* PROPERTIES *****************
    // MARK: ******************************************
    // MARK: - OUTSIDE PROPERTIES
    open var delegate:BYImagePickerViewDelegate?
    
    // MARK: - LOCAL PROPERTIES
    private enum SessionState {
        case ready
        case notAuthorized
        case setupFailure
    }
    
    private enum DeviceMode {
        case photo
        case video
    }

    fileprivate let outputMovieDirectoryPath  = "temp-video-files"
    fileprivate let outputMovieName           = "temp-video-"
    fileprivate let outputExtension           = ".mov"
    let fileManager = BYFileManager.sharedInstance
    // Communicate with the session and other session objets on this queue
    var sessionQueue = DispatchQueue(label: "com.chaos.byimagepicker-SessionQueue")
    let session = AVCaptureSession()
    
    var videoDeviceInput:AVCaptureDeviceInput! = nil
    var movieFileOutput:AVCaptureMovieFileOutput! = nil
    var imageOutput:AVCaptureStillImageOutput! = nil
    
    var backgroundRecordingID:UIBackgroundTaskIdentifier? = nil
    var outputMovieFileURL:URL!
    var flashMode:Bool = false
    var isFlashModeOn:Bool{
        get { return flashMode }
        set {
            let mode = (newValue) ? AVCaptureFlashMode.on : AVCaptureFlashMode.off
            flashMode = setDeviceFlashMode(device: videoDeviceInput.device, mode: mode)
        }
    }
    fileprivate var sessionState:SessionState                    = .ready
    fileprivate var deviceMode:DeviceMode                        = .photo
    fileprivate var videoDevicePosition:AVCaptureDevicePosition  = .back
    
    // MARK: - WIDGETS
    @IBOutlet weak var videoPreviewView:BYImagePickerPreviewView!
    @IBOutlet weak var flashB: UIButton!
    @IBOutlet weak var closeB: UIButton!
    @IBOutlet weak var galleryB: UIButton!
    @IBOutlet weak var switchB: UIButton!
    @IBOutlet weak var cameraV: BYImagePickerCameraView!

    // MARK: ******************************************
    // MARK: ************** METHODS *******************
    // MARK: ******************************************
    // MARK: - INITIALIZATION
    deinit {
        removeObservers()
    }
    
    fileprivate func initialize() {
        unarchiveAndInstantiateNib()
        setupWidgets()
        checkPermissions()
        outputMovieFileURL = createMovieOutputURL()
        
        sessionQueue.async { [unowned self] in
            self.setupCamera()
            self.addObservers()
        }
    }
    
    fileprivate func unarchiveAndInstantiateNib() {
        let controllerNib = BYImagePickerUtils.nib(nibName: "BYImagePickerViewController", owner: self)
        controllerNib.instantiate(withOwner: self, options: nil)
    }
    
    fileprivate func setupCamera() {
        if sessionState != .ready {
            print("Session is not ready to be configured.")
            return
        }
        
        videoPreviewView.session = session
        self.session.beginConfiguration()
        
        // Photo is initial `DeviceMode`
        if self.session.canSetSessionPreset(AVCaptureSessionPresetPhoto) {
            self.session.sessionPreset = AVCaptureSessionPresetPhoto
        }else {
            self.session.sessionPreset = AVCaptureSessionPresetHigh
        }
        
        // Add Video input
        var preferredVideoDevice:AVCaptureDevice! = nil
        if #available(iOS 10.0, *) {
            if let defaultVideoDevice = AVCaptureDevice.defaultDevice(withDeviceType: AVCaptureDeviceType.builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .back) {
                preferredVideoDevice = defaultVideoDevice
            }else if let defaultVideoDevice = AVCaptureDevice.defaultDevice(withDeviceType: AVCaptureDeviceType.builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .front) {
                preferredVideoDevice = defaultVideoDevice
            }
        } else {
            let devices = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as! [AVCaptureDevice]
            if let defaultVideoDevice = devices.filter({ $0.position == .back }).first {
                preferredVideoDevice = defaultVideoDevice
            }else if let defaultVideoDevice = devices.filter({ $0.position == .front }).first {
                preferredVideoDevice = defaultVideoDevice
            }
        }
        do {
            self.videoDeviceInput = try AVCaptureDeviceInput(device: preferredVideoDevice)
            if self.session.canAddInput(self.videoDeviceInput) {
                self.session.addInput(self.videoDeviceInput)
            }else {
                print("VideoDeviceInput could not be added to session! App shall be terminated." )
                self.session.commitConfiguration()
                return
            }
        }catch let error {
            print("VideoDeviceInput could not be created! App shall be terminated with error: \(error)")
            self.session.commitConfiguration()
            return
        }

        // Add Audio input
        if let audioDevices = AVCaptureDevice.devices(withMediaType: AVMediaTypeAudio) as? [AVCaptureDevice], let audioDevice = audioDevices.first {
            do {
                let audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice)
                if self.session.canAddInput(audioDeviceInput) {
                    self.session.addInput(audioDeviceInput)
                }else {
                    print("AudioDeviceInput could not be added to session! There will be no sound that recorded movies.")
                }
            }catch let error {
                print("AudioDeviceInput could not be created with error: \(error)")
            }
        }else {
            print("No available audio device!: There will be no sound that recorded movies.")
        }
        
        // Add Image Output
        self.imageOutput = self.createImageOutput()
        if self.session.canAddOutput(imageOutput) {
            self.session.addOutput(imageOutput)
        }else {
            print("ImageOutput could not be added to session! App shall be terminated.")
            self.session.commitConfiguration()
            return
        }
        
        self.isFlashModeOn = true
        self.setDeviceFocusMode(device: self.videoDeviceInput.device, pointOfInterest: CGPoint(x: 0.5, y: 0.5))
        self.setDeviceExposureMode(device: self.videoDeviceInput.device, pointOfInterest: CGPoint(x: 0.5, y: 0.5))
        self.session.commitConfiguration()
    }
    
    fileprivate func setupWidgets() {
        cameraV.delegate = self
        let flashImage   = BYImagePickerUtils.imageFromBundle(imageName: "flash-on")?.withRenderingMode(.alwaysOriginal)
        let closeImage   = BYImagePickerUtils.imageFromBundle(imageName: "close")?.withRenderingMode(.alwaysOriginal)
        let galleryImage = BYImagePickerUtils.imageFromBundle(imageName: "library")?.withRenderingMode(.alwaysOriginal)
        let switchImage  = BYImagePickerUtils.imageFromBundle(imageName: "rotate")?.withRenderingMode(.alwaysOriginal)
        
        flashB.setImage(flashImage, for: .normal)
        closeB.setImage(closeImage, for: .normal)
        galleryB.setImage(galleryImage, for: .normal)
        switchB.setImage(switchImage, for: .normal)
    }

    // MARK: - OVERRIDED METHODS
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sessionQueue.async {
            switch self.sessionState {
            case .ready:
                self.addObservers()
                self.session.startRunning()
                break
            case .notAuthorized:
                DispatchQueue.main.async { [unowned self] in
                    let text = "Camera permission is not granted, please change privacy settings"
                    let alertController = UIAlertController(title: "Camera", message: text, preferredStyle: .alert)
                    let actionOk = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    let actionSettings = UIAlertAction(title: "Settings", style: .`default`, handler: { (action) in
                        let deviceSettingsURL = URL(string: UIApplicationOpenSettingsURLString)!
                        UIApplication.shared.openURL(deviceSettingsURL)
                    })
                    self.present(alertController, animated: true, completion: nil)
                }
                
            case .setupFailure:
                DispatchQueue.main.async { [unowned self] in
                    let text = "Unabele to capture media"
                    let alertController = UIAlertController(title: "Setup Failure", message: text, preferredStyle: .alert)
                    let actionOk = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    self.present(alertController, animated: true, completion: nil)
                }
                
            }
        }
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        removeObservers()
        sessionQueue.async {
            self.session.stopRunning()
        }
        super.viewWillDisappear(animated)
    }
    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        print("viewWillTransition toSize: \(size)")
        if let videoPreviewLayerConnection = videoPreviewView.connection {
            let deviceOrientation = UIDevice.current.orientation
            guard let newOrientation = deviceOrientation.videoOrientation, deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
                print("Orientation is not one of the available's = Portrait od Landspace")
                return
            }
            videoPreviewLayerConnection.videoOrientation = newOrientation
        }
    }
    
    
    override open func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    open override var prefersStatusBarHidden: Bool {
        return true
    }
    
    open override var shouldAutorotate: Bool {
        return true
    }
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all // [UIInterfaceOrientationMask.portrait, UIInterfaceOrientationMask.landscape]
    }
    
    // MARK: - METHODS
    fileprivate func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.captureSessionNotificationHandler(notification:)), name: Notification.Name.AVCaptureSessionRuntimeError, object: session)
        NotificationCenter.default.addObserver(self, selector: #selector(self.captureSessionNotificationHandler(notification:)), name: Notification.Name.AVCaptureSessionWasInterrupted, object: session)
        NotificationCenter.default.addObserver(self, selector: #selector(self.captureSessionNotificationHandler(notification:)), name: Notification.Name.AVCaptureSessionInterruptionEnded, object: session)
        NotificationCenter.default.addObserver(self, selector: #selector(self.captureSessionNotificationHandler(notification:)), name: Notification.Name.AVCaptureDeviceSubjectAreaDidChange, object: videoDeviceInput.device)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.previewOrientationAdjustment(notification:)), name: Notification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    fileprivate func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }

//    func previewOrientationAdjustment(notification:Notification) {
//        if let videoPreviewLayerConnection = videoPreviewView.connection, videoPreviewLayerConnection.isVideoOrientationSupported {
//            let deviceOrientation = UIDevice.current.orientation
//            guard let newOrientation = deviceOrientation.videoOrientation, deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
//                print("Orientation is not one of the available's = Portrait od Landspace")
//                return
//            }
//            videoPreviewLayerConnection.videoOrientation = newOrientation
//        }
//    }
    
    fileprivate func setDeviceFocusMode(device: AVCaptureDevice, pointOfInterest: CGPoint)->Bool {
        do {
            try device.lockForConfiguration()
            if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(.continuousAutoFocus) {
                device.focusPointOfInterest = pointOfInterest
                device.focusMode = .continuousAutoFocus
                return true
            }else {
                device.focusMode = .locked
                print("Focus mode is not supported for device: \(device)")
                return false
            }
            device.unlockForConfiguration()
        }catch let error {
            print(error)
        }
        return false
    }
    
    fileprivate func setDeviceExposureMode(device: AVCaptureDevice, pointOfInterest: CGPoint)->Bool {
        do {
            try device.lockForConfiguration()
            if device.isFocusPointOfInterestSupported &&  device.isExposureModeSupported(.continuousAutoExposure) {
                device.exposurePointOfInterest = pointOfInterest
                device.exposureMode = .continuousAutoExposure
            }else {
                device.exposureMode = .locked
                print("Exposure mode is not supported for device: \(device)")
                return false
            }
        }catch let error {
            print(error)
        }
        return false
    }
    
    fileprivate func setDeviceFlashMode(device: AVCaptureDevice, mode:AVCaptureFlashMode)->Bool {
        do {
            if device.isFlashModeSupported(mode) {
                try device.lockForConfiguration()
                device.flashMode = mode
                device.unlockForConfiguration()
                return (device.flashMode == .on) ? true : false
            }else {
                print("Flash mode is not supported for device: \(device)")
                return false
            }
        }catch let error {
            print(error)
            return false
        }
    }
    
    fileprivate func checkPermissions() {
        switch AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) {
        case .authorized:
            // Nothing to do, user already has granted access to the camera.
            sessionState = .ready
            break
        case .notDetermined:
            // Audio access will be implicitly requested when we create
            // an AVCaptureDeviceInput for audio during session setup.
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { [unowned self] isGranted in
                if isGranted {
                    print("Video hardware access is granted!")
                    self.sessionQueue.resume()
                }else {
                    self.sessionState = .notAuthorized
                    print("Video hardware access if declined!")
                }
            })
            
        case .denied:
            sessionState = .notAuthorized
            print("Video hardware access is denied!")
        case .restricted:
            sessionState = .notAuthorized
            print("Video hardware access is restricted!")
            break
        }
    }
    
    fileprivate func toggleCamera() {
        var preferredPosition:AVCaptureDevicePosition! = nil
        switch videoDevicePosition {
        case .back:
            preferredPosition = .front
            
        case .front:
            preferredPosition = .back
            
        default:
            preferredPosition = .front
        }

        let currentVideoDevice = videoDeviceInput.device
        sessionQueue.async { [unowned self] in
            var newVideoDeviceInput:AVCaptureDeviceInput! = nil
            if let devices:[AVCaptureDevice] = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) as! [AVCaptureDevice]? {
                if let device = devices.filter({ $0.position == preferredPosition }).first {
                    do {
                        newVideoDeviceInput = try AVCaptureDeviceInput(device: device)
                        NotificationCenter.default.removeObserver(self,
                                                                  name: NSNotification.Name.AVCaptureDeviceSubjectAreaDidChange,
                                                                  object: self.videoDeviceInput.device)
                        NotificationCenter.default.addObserver(self,
                                                               selector: #selector(self.captureSessionNotificationHandler(notification:)),
                                                               name: Notification.Name.AVCaptureDeviceSubjectAreaDidChange,
                                                               object: newVideoDeviceInput.device)
                    }catch let error  {
                        print("Cannot create device input with device: \(device)")
                        return
                    }
                    
                    
                }else {
                    print("There is no devices with device position: \(preferredPosition)")
                    return
                }
            }else {
                print("There is no devices with media type: `AVMediaTypeVideo`")
                return
            }
        
            self.session.beginConfiguration()
            self.session.removeInput(self.videoDeviceInput)
            self.session.addInput(newVideoDeviceInput)
            self.session.commitConfiguration()
            self.videoDeviceInput = newVideoDeviceInput
            self.videoDevicePosition = preferredPosition
            self.isFlashModeOn = Bool(self.isFlashModeOn) // Check the flash mode state with new device
            
            DispatchQueue.main.async {
                self.setFlashState() // Update UI widgets
            }
        }
    }
    
    fileprivate func changeDeviceMode(mode:DeviceMode) {
        deviceMode = mode
        self.session.beginConfiguration()
        switch mode {
        case .photo:
            self.session.removeOutput(self.movieFileOutput)
            self.session.sessionPreset = AVCaptureSessionPresetPhoto
            self.movieFileOutput = nil
            
        case .video:
            if UIDevice.current.isMultitaskingSupported {
                self.backgroundRecordingID = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
            }
            let videoPreviewLayerConnection = videoPreviewView.previewLayer.connection
            let movieFileOutput = self.createMovieFileOutput()
            self.session.addOutput(movieFileOutput)
            self.session.sessionPreset = AVCaptureSessionPresetHigh
            if let connection = videoPreviewLayerConnection {
                let movieFileOutputConnection = movieFileOutput.connection(withMediaType: AVMediaTypeVideo)
                let movieVideoOrientation = connection.videoOrientation
                movieFileOutputConnection?.videoOrientation = movieVideoOrientation
                print("Video Orientation = \(connection.videoOrientation.rawValue) \(connection.videoOrientation)")
            }
            self.movieFileOutput = movieFileOutput
        }
        self.session.commitConfiguration()
    }
    
    fileprivate func setFlashState() {
        if self.isFlashModeOn {
            let image = BYImagePickerUtils.imageFromBundle(imageName: "flash-on")?.withRenderingMode(.alwaysOriginal)
            self.flashB.setImage(image, for: .normal)
        }else {
            let image = BYImagePickerUtils.imageFromBundle(imageName: "flash-off")?.withRenderingMode(.alwaysOriginal)
            self.flashB.setImage(image, for: .normal)
        }
    }
    
    fileprivate func focus(touchPoint:CGPoint) {
        sessionQueue.async { [unowned self] in
            let convertedPointOfInterest = self.videoPreviewView.previewLayer.captureDevicePointOfInterest(for: touchPoint)
            self.setDeviceFocusMode(device: self.videoDeviceInput.device, pointOfInterest: convertedPointOfInterest)
            self.setDeviceExposureMode(device: self.videoDeviceInput.device, pointOfInterest: convertedPointOfInterest)
            DispatchQueue.main.async {
                self.videoPreviewView.playAnimation(center: touchPoint)
            }
        }
    }

    // MARK: - DELEGATES
    // MARK: - ACTIONS
    @IBAction func flash_Action(_ sender: UIButton) {
        isFlashModeOn = !isFlashModeOn
        setFlashState()
    }
    @IBAction func close_Action(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func gallery_Action(_ sender: UIButton) {
        var imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            imagePicker.sourceType = .savedPhotosAlbum
            let savedPhotosAlbumMediaTypes = UIImagePickerController.availableMediaTypes(for: .savedPhotosAlbum)
            imagePicker.mediaTypes = savedPhotosAlbumMediaTypes!
        }else {
            imagePicker.sourceType = .photoLibrary
            let photoLibraryMediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)
            imagePicker.mediaTypes = photoLibraryMediaTypes!
        }
        imagePicker.modalTransitionStyle = .coverVertical
        imagePicker.modalPresentationStyle = .fullScreen
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func switch_Action(_ sender: UIButton) {
        toggleCamera()
    }
    
    @IBAction func tapFocus_Action(_ gesture: UITapGestureRecognizer) {
        let touchPoint = gesture.location(in: videoPreviewView)
        focus(touchPoint: touchPoint)
        let convertedPointOfInterest = videoPreviewView.previewLayer.captureDevicePointOfInterest(for: touchPoint)
    }
    
    @objc fileprivate func captureSessionNotificationHandler(notification:Notification?) {
        switch notification!.name {
        case Notification.Name.AVCaptureSessionRuntimeError:
            session.startRunning()
            print("AVCaptureSessionRuntimeError")
            
        case Notification.Name.AVCaptureSessionWasInterrupted:
            session.stopRunning()
            print("Phone or FaceCall started. ")
            print("AVCaptureSessionWasInterrupted")
            
        case Notification.Name.AVCaptureSessionInterruptionEnded:
            session.startRunning()
            print("Phone or FaceCall ended.")
            print("AVCaptureSessionInterruptionEnded")

        case Notification.Name.AVCaptureDeviceSubjectAreaDidChange:
            print("AVCaptureDeviceSubjectAreaDidChange")
            
        case Notification.Name.UIDeviceOrientationDidChange:
            print("UIDeviceOrientationDidChange")
            
        default:
            print("Unhandled session notification")
            
        }
    }
    
    // MARK: - HELPERS
    fileprivate func createImageOutput()->AVCaptureStillImageOutput {
        let imageOutput = AVCaptureStillImageOutput()
        let outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
        imageOutput.outputSettings = outputSettings
        return imageOutput
    }
    
    fileprivate func createMovieFileOutput()->AVCaptureMovieFileOutput {
        let time = CMTime(value: 60, timescale: 1) // (1 sec * 60/1)
        let movieFileOutput = AVCaptureMovieFileOutput()
        movieFileOutput.maxRecordedDuration = time
        return movieFileOutput
    }
    
    fileprivate func createMovieOutputURL()->URL {
        var cacheDirectoryURL = fileManager.cacheDirectoryURL
        cacheDirectoryURL?.appendPathComponent(outputMovieDirectoryPath)
        fileManager.removeFiles(atDiractoryURL: cacheDirectoryURL!)
        if !fileManager.isFileExits(atURL: cacheDirectoryURL!) {
            if fileManager.createDirectory(atURL: cacheDirectoryURL!) {
                print("Created successfully!")
            }else {
                print("Directory creation is failed!")
            }
        }
        let outputFileName = generateMovieFileName()
        cacheDirectoryURL?.appendPathComponent(outputFileName)
        return cacheDirectoryURL!
    }
    
    fileprivate func getImageConnection()->AVCaptureConnection? {
        var videoConnection:AVCaptureConnection? = nil
        for connection in imageOutput.connections as! [AVCaptureConnection] {
            for inputPort in connection.inputPorts as! [AVCaptureInputPort] {
                if inputPort.mediaType == AVMediaTypeVideo {
                    return connection
                }
            }
        }
        
        return nil
    }

    fileprivate func generateMovieFileName()->String {
        let currentTime:TimeInterval = Date().timeIntervalSince1970
        let UIDPostfix:String = String(currentTime.hashValue)
        let fileName:String = outputMovieName + UIDPostfix + outputExtension
        return fileName
    }
}
// MARK: - BYImagePickerCameraViewDelegate
extension BYImagePickerViewController: BYImagePickerCameraViewDelegate {
    func cameraView(takePhoto cameraView: BYImagePickerCameraView) {
        let videoConnection = getImageConnection()
        if let connection = videoConnection {
            connection.videoOrientation = self.videoPreviewView.connection!.videoOrientation
            imageOutput.captureStillImageAsynchronously(from: connection, completionHandler: { (sampleBuffer, error) in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                let image = UIImage(data: imageData!)
                switch image!.imageOrientation {
                case .right:
                    break
                case .up:
                    break
                case .down:
                    break
                default:
                    break
                }
                
                let vc = BYImagePickerPhotoPreview(nibName: "BYImagePickerPhotoPreview", bundle: BYImagePickerUtils.bundle())
                vc.delegate = self.delegate
                vc.image = image
                self.navigationController?.pushViewController(vc, animated: true)
            })
        }
    }
    
    func cameraView(videoRecordStarted cameraView: BYImagePickerCameraView) {
        sessionQueue.async { [unowned self] in
            self.changeDeviceMode(mode: .video)
            self.movieFileOutput.startRecording(toOutputFileURL: self.outputMovieFileURL, recordingDelegate: self)
        }
    }
    
    func cameraView(videoRecordStopped cameraView: BYImagePickerCameraView) {
        self.movieFileOutput.stopRecording()
    }
}

// MARK: - AVCaptureFileOutputRecordingDelegate
extension BYImagePickerViewController: AVCaptureFileOutputRecordingDelegate {
    public func capture(_ captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
        print("Video recording did started!")
    }
    
    public func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        if error != nil {
            let averror:AVError = error as! AVError
            let isSuccessed = averror.userInfo[AVErrorRecordingSuccessfullyFinishedKey] as! Bool
            if isSuccessed {
                print("Successed! with error: \(averror.code)")
            }else {
                switch averror.code {
                case .diskFull:
                    print("diskFull")
                    
                case .maximumDurationReached:
                    print("maximumDurationReached")
                    
                case .maximumFileSizeReached:
                    print("maximumFileSizeReached")
                    
                default:
                    print("Default: Unimplemented error code: \(averror.code)")
                
                }

            }
        }else {
            print("Successed without error!")
        }
        
        if let backgroundTask = backgroundRecordingID {
            UIApplication.shared.endBackgroundTask(backgroundTask)
        }

        let videoOrientation = movieFileOutput.connection(withMediaType: AVMediaTypeVideo)!.videoOrientation
        let vc  = BYImagePickerVideoPreview(nibName: "BYImagePickerVideoPreview", bundle: BYImagePickerUtils.bundle())
        vc.delegate = delegate
        vc.videoURL = outputFileURL
        vc.videoOrientation = videoOrientation
        sessionQueue.async { [unowned self] in
            self.changeDeviceMode(mode: .photo)
        }
        navigationController?.pushViewController(vc, animated: false)
    }
}

// MARK: - UIIMagePickerViewDelegate
extension BYImagePickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        if mediaType == String(kUTTypeMovie) {
            let mediaURL = info[UIImagePickerControllerMediaURL] as! URL
            let referenceURL = info[UIImagePickerControllerReferenceURL] as! URL
        }else if mediaType == String(kUTTypeImage) {
            let originalImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
