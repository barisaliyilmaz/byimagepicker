//
//  BYImagePickerPreviewView.swift
//  Pods
//
//  Created by Baris Yilmaz on 14/12/2016.
//
//

import UIKit
import AVFoundation

class BYImagePickerPreviewView: UIView {
    var previewLayer: AVCaptureVideoPreviewLayer {
        return layer as! AVCaptureVideoPreviewLayer
    }
    
    var connection: AVCaptureConnection? {
        return previewLayer.connection
    }
    
    var session: AVCaptureSession? {
        get {
            return previewLayer.session
        }
        set {
            previewLayer.session = newValue
        }
    }
    
    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }
    
    // MARK: - LOCALE PROPERTIES
    var focusSquare:UIView!
    var focusSquareAnimation:CABasicAnimation!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        
        focusSquare = UIView(frame: CGRect(x: 0, y: 0, width: 164, height: 164))
        focusSquare.layer.backgroundColor = UIColor.clear.cgColor
        focusSquare.layer.borderWidth = 1
        focusSquare.layer.borderColor = UIColor.clear.cgColor
        
        focusSquareAnimation = CABasicAnimation(keyPath: "borderColor")
        focusSquareAnimation.fromValue = UIColor.clear.cgColor
        focusSquareAnimation.toValue = UIColor(red: 251/255, green: 188/255, blue: 5/255, alpha: 1.0).cgColor
        focusSquareAnimation.repeatCount = 3
        focusSquareAnimation.duration = 0.3
        
        addSubview(focusSquare)
    }
    
    open func playAnimation(center:CGPoint) {
        focusSquare.center = center
        focusSquare.layer.add(focusSquareAnimation, forKey: "focusSquareAnimation")
    }
}
