//
//  BYMessageTextView.swift
//  Pods
//
//  Created by Baris Yilmaz on 16/12/2016.
//
//

import UIKit

open class BYMessageTextView: UITextView {
    
    // Constants
    let cornerRadiusConst:CGFloat = 5.0
    let scrollIndicatorInsetsConst = UIEdgeInsets(top: 5, left: 2, bottom: 5, right: 2.0)
    let textContainerInsetConst = UIEdgeInsets(top: 10, left: 2, bottom: 10, right: 2)
    let contentInsetConst = UIEdgeInsets(top: 1, left: 0, bottom: 1, right: 0)
    let doneCharacter = "\n"
    
    @IBOutlet weak var heightConstraint:NSLayoutConstraint!
    @IBOutlet weak var minHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var maxHeightConstraint:NSLayoutConstraint!
    
    // Properties
    @IBInspectable
    var placeHolder:String = "New Message"
    
    @IBInspectable
    var placeHolderColor:UIColor = UIColor.lightGray
    
    @IBInspectable
    var borderWidth:CGFloat {
        get {
            return self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor:UIColor {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable
    var cornerRadius:CGFloat {
        get {
            return self.layer.cornerRadius
        }
        
        set {
            self.layer.cornerRadius = newValue
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        
        setupTextView()
    }
    
    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if (text.characters.count == 0 && !placeHolder.isEmpty) {
            placeHolderColor.set()
            let lineHeight = self.font?.lineHeight
            let textViewHeight = self.bounds.height
            var midY:CGFloat = (textViewHeight - lineHeight!) / 2
            midY = CGFloat(ceilf(Float(midY)))
            let rectWithOffset = CGRect(origin: CGPoint(x: 7, y: midY), size: frame.size)
            NSString(string: placeHolder).draw(in: rectWithOffset, withAttributes: placeholderTextAttributes())
        }
    }
    
    override open var bounds: CGRect{
        didSet {
            if contentSize.height <= bounds.height + 1 {
                contentOffset = .zero
            }
        }
    }
    
    fileprivate func setupTextView() {
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.isDirectionalLockEnabled = true
        self.layer.cornerRadius = cornerRadiusConst
        self.layer.masksToBounds = true
        self.scrollIndicatorInsets = scrollIndicatorInsetsConst
        self.textContainerInset = textContainerInsetConst
        self.contentInset = contentInsetConst
        self.contentMode = .redraw
        self.dataDetectorTypes = .all
        self.keyboardType = .default
//        self.returnKeyType = .default
        self.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(BYMessageTextView.textDidChange(notification:)), name: Notification.Name.UITextViewTextDidChange, object: self)
    }
    
    fileprivate func placeholderTextAttributes()->[String:AnyObject] {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = textAlignment
        let dic:[String:AnyObject] = [NSFontAttributeName: font!,
                                      NSForegroundColorAttributeName: placeHolderColor,
                                      NSParagraphStyleAttributeName: paragraphStyle]
        return dic
    }
    
    fileprivate func adjustHeightConstraint() {
        // Calculate text size which allows to use it without scrolling.
        var newHeight = self.sizeThatFits(frame.size).height
        newHeight = min(newHeight, maxHeightConstraint.constant)
        newHeight = max(newHeight, minHeightConstraint.constant)
        heightConstraint.constant = newHeight
    }
    
    // MARK - Actions
    func textDidChange(notification:Notification) {
        adjustHeightConstraint()
        setNeedsDisplay()
    }
    
    // MARK - Open Methods
    override open var hasText: Bool {
        let stringWithoutWhitespaceAndNewline = text.trimmingCharacters(in: .whitespacesAndNewlines)
        return (stringWithoutWhitespaceAndNewline.characters.count > 0) ? true : false
    }
}

extension BYMessageTextView: UITextViewDelegate {
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if doneCharacter == text {
            resignFirstResponder()
            return false
        }
        return true
    }
}
