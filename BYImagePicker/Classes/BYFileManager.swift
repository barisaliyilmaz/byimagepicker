//
//  BYFileManager.swift
//  Pods
//
//  Created by Baris Yilmaz on 09/12/2016.
//
//

import UIKit

class BYFileManager: NSObject {
    static let sharedInstance = BYFileManager()
    
    fileprivate let fileManager = FileManager.default
    internal var cacheDirectoryURL:URL!
    internal var documentDirectoryURL:URL!
    
    fileprivate override init() {
        super.init()
        
        initialize()
    }
    
    // MARK: - INITIALIZATION
    fileprivate func initialize() {
        cacheDirectoryURL = try! fileManager.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        documentDirectoryURL = try! fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        print(cacheDirectoryURL)
        print(documentDirectoryURL)
    }
    
    // MARK: - METHODS
    internal func isFileExits(atURL url:URL)->Bool {
        let result:Bool = fileManager.fileExists(atPath: url.path)
        return result
    }
    
    internal func isFileExist(atDirectoryURL url:URL, fileName:String)->Bool {
        let urlWithFileName = url.appendingPathComponent(fileName, isDirectory: false)
        let result:Bool = fileManager.fileExists(atPath: urlWithFileName.path)
        return result
    }
    
    internal func createFile(atDirectoryURL url:URL, fileName:String)->Bool {
        var directoryURL = url
        directoryURL = directoryURL.appendingPathComponent(fileName, isDirectory: false)
        do {
            try fileManager.createDirectory(at: directoryURL, withIntermediateDirectories: true, attributes: nil)
        }catch let error as NSError{
            debugPrint(error)
            return false
        }
        return true
    }
    
    internal func createDirectory(atURL url:URL)->Bool {
        do {
            try fileManager.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        }catch let error {
            print(error)
            return false
        }
        return true
    }
    
    internal func removeFile(atURL url:URL, withFileName fileName:String)->Bool {
        var fileURL = url
        fileURL.appendPathComponent(fileName)
        if fileManager.fileExists(atPath: fileURL.path) {
            do {
                try fileManager.removeItem(at: fileURL)
            }catch let error {
                print(error)
                return false
            }
        }
        return true
    }
    
    internal func removeFiles(atDiractoryURL url:URL)->Bool {
        let resourceKeys = [URLResourceKey.fileResourceTypeKey, URLResourceKey.isDirectoryKey, URLResourceKey.nameKey]
        let directoryEnum = fileManager.enumerator(at: url, includingPropertiesForKeys: resourceKeys)
        
        for case let fileURL as NSURL in directoryEnum! {
            guard let resourceValues = try? fileURL.resourceValues(forKeys: resourceKeys),
                  let isDirectory = resourceValues[URLResourceKey.isDirectoryKey] as? Bool,
                  let name = resourceValues[URLResourceKey.nameKey] as? String
            else {
                continue
            }
            
            if isDirectory == false {
                print("File is removing: \(name)")
                removeFile(atURL: fileURL as! URL)
            }
        }
        
        return true
    }
    
    internal func removeFile(atURL fileURL:URL)->Bool {
        if fileManager.fileExists(atPath: fileURL.path) {
            do {
                try fileManager.removeItem(at: fileURL)
            }catch let error {
                print(error)
                return false
            }
        }
        return true
    }
}
