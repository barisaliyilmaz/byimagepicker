//
//  BYImagePickerProfiler.swift
//  Pods
//
//  Created by Baris Yilmaz on 20/12/2016.
//
//

import UIKit

class BYImagePickerProfiler: NSObject {
    static var profileNameDic = [String:CFTimeInterval]()
    
    class func start(name:String) {
        let currentTime:CFTimeInterval = CACurrentMediaTime()
        profileNameDic[name] = currentTime
        print("Profile Name: \(name) started.")
    }
    
    class func end(name:String) {
        let currentTime = CACurrentMediaTime()
        let elapsedTime = currentTime - profileNameDic[name]!
        print("Profile Name: \(name) => \(elapsedTime) sec.")
    }
}
