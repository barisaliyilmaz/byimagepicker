//
//  BYImagePickerUtils.swift
//  BYImagePicker
//
//  Created by Baris Yilmaz on 08/12/2016.
//  Copyright © 2016 Baris Yilmaz. All rights reserved.
//

import UIKit

private struct BYAssetBundleStaticStruct {
    static var Bundle:Bundle!
    static var AssetBundle:Bundle!
}

open class BYImagePickerUtils: NSObject {
    class open func bundle()->Bundle {
        if BYAssetBundleStaticStruct.Bundle == nil {
            let messageAppBundle = Bundle(for: BYImagePickerViewController.self)
            let messageAppBundleURL = messageAppBundle.resourceURL!
            BYAssetBundleStaticStruct.Bundle = Bundle(url: messageAppBundleURL)
        }
        return BYAssetBundleStaticStruct.Bundle
    }
    
    class open func assetBundle()->Bundle {
        if BYAssetBundleStaticStruct.AssetBundle == nil {
            let bundle = self.bundle()
            let assetBundlePath = bundle.path(forResource: "BYImagePicker", ofType: "bundle")
            BYAssetBundleStaticStruct.AssetBundle = Bundle(path: assetBundlePath!)
        }
        return BYAssetBundleStaticStruct.AssetBundle
    }
    
    class open func imageFromBundle(imageName:String)->UIImage? {
        let assetBundle = self.assetBundle()
        let imageWithDirectory = "images/" + imageName
        let image = UIImage(named: imageWithDirectory, in: assetBundle, compatibleWith: nil)
        return image
    }
    
    class open func nib(nibName name:String, owner:Any? = nil)->UINib {
        let bundle = self.bundle()
        let nib = UINib(nibName: name, bundle: bundle)
        return nib
    }
}
