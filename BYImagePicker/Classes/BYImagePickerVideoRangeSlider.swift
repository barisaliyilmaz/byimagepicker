//
//  BYImagePickerVideoRangeSlider.swift
//  Pods
//
//  Created by Baris Yilmaz on 12/20/16.
//
//

import UIKit

class BYImagePickerVideoRangeSlider: UIView {

    // MARK: ******************************************
    // MARK: ************* PROPERTIES *****************
    // MARK: ******************************************
    // MARK: - OUTSIDE PROPERTIES
    var imageSequence:[UIImage]! = nil {
        didSet {
            setupImageViews()
        }
    }
    var target:Any?
    var selector:Selector?
    
    // MARK: - LOCAL PROPERTIES
    var imageViewList:[UIImageView] = [UIImageView]()
    var itemCount:Int = 0
    var itemSize:CGSize = CGSize.zero
    var startX:CGFloat = 0.0
    var unitPos:CGFloat {
        get {
            return getUnitPosition()
        }
        set {
            let position:CGFloat = newValue * frame.width
            var theFrame = thumbnailView.frame
            theFrame.origin.x = position
            thumbnailView.frame = theFrame
        }
    }
    
    // MARK: - WIDGETS
    var thumbnailView:UIImageView!
    
    // MARK: ******************************************
    // MARK: ************** METHODS *******************
    // MARK: ******************************************
    // MARK: - INITIALIZATION
    fileprivate func initialize() {
        self.isUserInteractionEnabled = true
        backgroundColor     = UIColor.gray
        layer.shadowOffset  = CGSize(width: -1, height: -1)
        layer.shadowColor   = UIColor.black.cgColor
        layer.shadowRadius  = 6.0
        
        setupTumbnailView()
        itemCount = calculateItemCount()
        itemSize = CGSize(width: self.frame.height, height: self.frame.height)
    }
    
    fileprivate func setupImageViews() {
        for subview in self.subviews {
            if subview === thumbnailView {
                continue
            }
            subview.removeFromSuperview()
        }
        
        for i in 0..<itemCount {
            let posX = itemSize.width * CGFloat(i)
            let posY:CGFloat = 0
            let imageView = UIImageView(frame: CGRect(x: posX, y: posY, width: itemSize.width, height: itemSize.height))
            imageView.contentMode = .scaleAspectFill
            imageView.layer.masksToBounds = true
            if i < imageSequence.count {
                imageView.image = imageSequence[i]
                addSubview(imageView)
            }
        }
        bringSubview(toFront: thumbnailView)
    }

    fileprivate func setupTumbnailView() {
        thumbnailView = UIImageView(image: BYImagePickerUtils.imageFromBundle(imageName: "slider-thumbnail"))
        thumbnailView.isUserInteractionEnabled = true
        thumbnailView.layer.masksToBounds = false
        let center = self.convert(self.center, from: self.superview!)
        thumbnailView.center = center
        var theFrame = thumbnailView.frame
        theFrame.origin.x = 0
        thumbnailView.frame = theFrame
        addSubview(thumbnailView)
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.dragging(gesture:)))
        gesture.minimumNumberOfTouches = 1
        gesture.maximumNumberOfTouches = 1
        thumbnailView.addGestureRecognizer(gesture)
    }
    
    // MARK: - OVERRIDED METHODS
    override func awakeFromNib() {
        initialize()
    }
    
    // MARK: - METHODS
    internal func addAction(target:Any, selector:Selector) {
        self.target = target
        self.selector = selector
    }
    
    internal func removeAction() {
        self.target = nil
        self.selector = nil
    }
    
    public func updateLayout() {
        itemCount = calculateItemCount()
        itemSize = CGSize(width: self.frame.height, height: self.frame.height)
        setupImageViews()
    }
    
    // MARK: - DELEGATES
    
    // MARK: - ACTIONS
    @objc fileprivate func dragging(gesture:UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            let location = gesture.location(in: self)
            startX = location.x - thumbnailView.frame.halfWidth
            var theFrame = gesture.view?.frame
            theFrame?.origin.x = startX
            gesture.view?.frame = theFrame!
            
            if let target = self.target, let selector = self.selector {
                let objectTarget = target as! NSObjectProtocol
                objectTarget.perform(selector, with: self)
            }
            
        case .changed:
            let translation = gesture.translation(in: self)
            var theFrame = gesture.view?.frame
            theFrame?.origin.x = startX + translation.x
            if theFrame!.origin.x < CGFloat(0) {
                theFrame!.origin.x = 0
            }else if theFrame!.origin.x > (frame.width - theFrame!.width) {
                theFrame!.origin.x = frame.width - theFrame!.width
            }
            gesture.view?.frame = theFrame!
            
            if let target = self.target, let selector = self.selector {
                let objectTarget = target as! NSObjectProtocol
                objectTarget.perform(selector, with: self)
            }
        case .ended:
            break
            
        default:
            print("default")
        }
    }
    
    // MARK: - HELPERS
    fileprivate func getUnitPosition()->CGFloat {
        let unitPos = thumbnailView.frame.origin.x / frame.width
        return unitPos
    }
    
    fileprivate func calculateItemCount()->Int {
        var itemCount = Int(self.frame.width / self.frame.height)
        let remainderItem = self.frame.width.truncatingRemainder(dividingBy: self.frame.height)
        if remainderItem > 0 {
            itemCount += 1
        }
        return itemCount
    }
}
