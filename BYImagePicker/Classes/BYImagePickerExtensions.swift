//
//  BYImagePickerExtensions.swift
//  Pods
//
//  Created by Baris Yilmaz on 13/12/2016.
//
//

import UIKit
import AVFoundation

extension CGRect {
    var halfWidth:CGFloat {
        return width / 2
    }
    
    var halfHeight:CGFloat {
        return height / 2
    }
}

extension CGSize {
    var halfWidth:CGFloat {
        return width / 2
    }
    
    var halfHeight:CGFloat {
        return height / 2
    }
}

extension UIDeviceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeRight
        case .landscapeRight: return .landscapeLeft
        default: return nil
        }
    }
}

extension UIInterfaceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeLeft
        case .landscapeRight: return .landscapeRight
        default: return nil
        }
    }
}

@available(iOS 10.0, *)
extension AVCaptureDeviceDiscoverySession {
    func uniqueDevicePositionsCount() -> Int {
        var uniqueDevicePositions = [AVCaptureDevicePosition]()
        
        for device in devices {
            if !uniqueDevicePositions.contains(device.position) {
                uniqueDevicePositions.append(device.position)
            }
        }
        return uniqueDevicePositions.count
    }
}

extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
    var radiansToDegrees: Double { return Double(self) * 180 / .pi }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}

extension UIImage {
    func resizeImage(newWidth: CGFloat)->UIImage {
        let scale = newWidth / size.width
        let newHeight = size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func cropImage(toSize size:CGSize)->UIImage {
        let refWidth:CGFloat = CGFloat(self.cgImage!.width)
        let refHeight:CGFloat = CGFloat(self.cgImage!.height)
        
        let x = (refWidth - size.width) / 2.0
        let y = (refHeight - size.height) / 2.0
        
        let cropRect = CGRect(x: x, y: y, width: size.height, height: size.width)
        let imageRef = self.cgImage!.cropping(to: cropRect)
        let cropped = UIImage(cgImage: imageRef!, scale: 0.0, orientation: self.imageOrientation)
        
        return cropped
    }
    
    func rotated(by degrees: Double) -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        
        let transform = CGAffineTransform(rotationAngle: CGFloat(degrees.degreesToRadians))
        var rect = CGRect(origin: .zero, size: self.size).applying(transform)
        rect.origin = .zero
        var selfRect = CGRect(origin: .zero, size: self.size)
        
        var image:UIImage! = nil
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(size: selfRect.size)
            image = renderer.image { renderContext in
                renderContext.cgContext.translateBy(x: selfRect.midX, y: selfRect.midY)
                renderContext.cgContext.rotate(by: CGFloat(degrees.degreesToRadians))
                renderContext.cgContext.scaleBy(x: 1.0, y: -1.0)
                
                let drawRect = CGRect(origin: CGPoint(x: -rect.size.width/2, y: -rect.size.height/2), size: rect.size)
                renderContext.cgContext.draw(cgImage, in: drawRect)
            }
        } else {
            UIGraphicsBeginImageContext(rect.size)
            let context = UIGraphicsGetCurrentContext()
            if let  bitmap = context {
                bitmap.translateBy(x: rect.midX, y: rect.midY)
                bitmap.rotate(by: CGFloat(degrees.degreesToRadians))
                bitmap.scaleBy(x: 1.0, y: -1.0)
                
                let drawRect = CGRect(origin: CGPoint(x: -self.size.width/2, y: -self.size.height/2), size: self.size)
                bitmap.draw(self.cgImage!, in: drawRect)
                image = UIGraphicsGetImageFromCurrentImageContext()
            }else {
                assert(false, "Bitmap context could not be created.")
            }
            UIGraphicsEndImageContext()
        }
        
        return image
    }
}

extension AVAsset {
    func orientationForTrack()->UIInterfaceOrientation {
        let videoTrack = self.tracks(withMediaType: AVMediaTypeVideo).first!
        let size = videoTrack.naturalSize
        let txf = videoTrack.preferredTransform
        
        if size.width == txf.tx && size.height == txf.ty {
            return UIInterfaceOrientation.landscapeRight
        }else if txf.tx == 0 && txf.ty == 0 {
            return UIInterfaceOrientation.landscapeLeft
        }else if txf.tx == 0 && txf.ty == size.width {
            return UIInterfaceOrientation.portraitUpsideDown
        }else {
            return UIInterfaceOrientation.portrait
        }
    }
    
    func rotateVideo(completion:(_ mutableComposition:AVMutableComposition, _ mutableVideoComposition:AVMutableVideoComposition)->Void) {
        
        var instruction:AVMutableVideoCompositionInstruction! = nil
        var layerInstruction:AVMutableVideoCompositionLayerInstruction! = nil
        var t1:CGAffineTransform! = nil
        var t2:CGAffineTransform! = nil
        
        var assetVideoTrack:AVAssetTrack! = nil
        var assetAudioTrack:AVAssetTrack! = nil
        
        if self.tracks(withMediaType: AVMediaTypeVideo).count > 0 {
            assetVideoTrack = self.tracks(withMediaType: AVMediaTypeVideo).first!
        }
        if self.tracks(withMediaType: AVMediaTypeAudio).count > 0 {
            assetAudioTrack = self.tracks(withMediaType: AVMediaTypeAudio).first!
        }
        
        let insertionPoint = kCMTimeZero
        
        // Create composition with the given asset and insert audio, video tracks into it from asset
        let mutableComposition = AVMutableComposition()
        let videoTimeRange = CMTimeRange(start: kCMTimeZero, duration: self.duration)
        let audioTimeRange = CMTimeRange(start: kCMTimeZero, duration: self.duration)
        
        let compositionVideoTrack = mutableComposition.addMutableTrack(withMediaType: AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
        try! compositionVideoTrack.insertTimeRange(videoTimeRange, of: assetVideoTrack, at: insertionPoint)
        
        let compositionAudioTrack = mutableComposition.addMutableTrack(withMediaType: AVMediaTypeAudio, preferredTrackID: kCMPersistentTrackID_Invalid)
        try! compositionAudioTrack.insertTimeRange(audioTimeRange, of: assetAudioTrack, at: insertionPoint)
        
        t1 = CGAffineTransform(translationX: assetVideoTrack.naturalSize.height, y: 0.0)
        let radian:CGFloat = CGFloat((90.0).degreesToRadians)
        t2 = t1.rotated(by: radian)
        
        // Set the appropriate render size and rotational transform
        let mutableVideoComposition = AVMutableVideoComposition()
        mutableVideoComposition.renderSize = CGSize(width: assetVideoTrack.naturalSize.height, height: assetVideoTrack.naturalSize.width)
        mutableVideoComposition.frameDuration = CMTimeMake(1, 30)
        
        // The rotate transform is set on a layer instruction
        instruction = AVMutableVideoCompositionInstruction()
        instruction.timeRange = CMTimeRange(start: kCMTimeZero, duration: mutableComposition.duration)
        layerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: mutableComposition.tracks.first!)
        layerInstruction.setTransform(t2, at: kCMTimeZero)
        
        // Add the transform instructions to the video composition
        instruction.layerInstructions = [layerInstruction]
        mutableVideoComposition.instructions = [instruction]
        
        completion(mutableComposition, mutableVideoComposition)
    }
    
    func exportVideo(asset:AVAsset, videoComposition:AVMutableVideoComposition, completion:@escaping (_ transcodedVideoURL:URL)->Void) {
        
        let cacheDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first!
        var cacheURL = URL(fileURLWithPath: cacheDirectory)
        cacheURL.appendPathComponent("temp-video.mp4")
        
        if FileManager.default.fileExists(atPath: cacheURL.path) {
            do {
                try FileManager.default.removeItem(at: cacheURL)
            }catch let error {
                debugPrint(error)
            }
        }
        
        let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetLowQuality)
        exportSession!.videoComposition = videoComposition
        exportSession!.outputURL = cacheURL
        exportSession!.outputFileType = AVFileTypeQuickTimeMovie
        exportSession!.exportAsynchronously {
            switch exportSession!.status {
            case .completed:
                completion(exportSession!.outputURL!)
                break
            case .failed:
                break
            default:
                break
            }
        }
    }
}
