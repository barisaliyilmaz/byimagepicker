//
//  BYImagePickerCameraView.swift
//  Pods
//
//  Created by Baris Yilmaz on 12/12/2016.
//
//

import UIKit

protocol BYImagePickerCameraViewDelegate {
    func cameraView(videoRecordStarted cameraView:BYImagePickerCameraView)
    func cameraView(videoRecordStopped cameraView:BYImagePickerCameraView)
    func cameraView(takePhoto cameraView:BYImagePickerCameraView)
}

fileprivate struct AnimationData {
    enum AnimationState {
        case start
        case begin
        case end
    }
    
    var beginStateScaleFactor:CGFloat = 1.5
    var endStateScaleFactor:CGFloat   = 2.0
    
    var duration:CGFloat = 1.0
    var delay:CGFloat = 0.0
    var springWithDamping:CGFloat = 0.2
    var springVelocity:CGFloat = 0.0
    var animationOption:UIViewAnimationOptions = .curveEaseOut
    
    var targetViewBounds:CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
    var state:AnimationState = .start
    
    init(targetViewBounds:CGRect) {
        self.targetViewBounds = targetViewBounds
    }
}

open class BYImagePickerCameraView: UIView {
    // MARK: ******************************************
    // MARK: ************* PROPERTIES *****************
    // MARK: ******************************************
    // MARK: - OUTSIDE PROPERTIES
    
    // MARK: - LOCAL PROPERTIES
    var delegate:BYImagePickerCameraViewDelegate?
    fileprivate var imageView:UIImageView!
    fileprivate var animatedRecordingView:UIView!
    fileprivate var recordingView:UIView!
    fileprivate var isRecordingModeOn = false
    fileprivate var isTouching = false
    fileprivate var animationData:AnimationData!
    
    // MARK: - WIDGETS
    
    // MARK: ******************************************
    // MARK: ************** METHODS *******************
    // MARK: ******************************************
    // MARK: - INITIALIZATION
    open func initialize() {
        isUserInteractionEnabled = true
        backgroundColor          = UIColor.clear
        layer.masksToBounds      = false
        
        setupWidgets()
    }
    
    fileprivate func setupWidgets() {
        imageView = UIImageView()
        imageView.image = BYImagePickerUtils.imageFromBundle(imageName: "camera")
        imageView.contentMode = .scaleAspectFit
        imageView.sizeToFit()
        self.addSubview(imageView)
        
        var recordingViewBounds = imageView.bounds
        recordingViewBounds.size.width -= 16
        recordingViewBounds.size.height -= 16
        recordingView = UIImageView(frame: recordingViewBounds)
        recordingView.backgroundColor = UIColor.red
        recordingView.alpha = 0.0
        recordingView.layer.cornerRadius = recordingView.frame.halfWidth
        let localMidPoint = self.convert(self.center, from: self.superview!)
        recordingView.center = localMidPoint
        self.addSubview(recordingView)
        
        animationData = AnimationData(targetViewBounds: imageView.bounds)
        animatedRecordingView = UIView(frame: animationData.targetViewBounds)
        animatedRecordingView.alpha = 0.0
        animatedRecordingView.backgroundColor = UIColor.red
        animatedRecordingView.layer.cornerRadius = animatedRecordingView.bounds.halfWidth
        self.addSubview(animatedRecordingView)
        sendSubview(toBack: animatedRecordingView)
    }
    
    // MARK: - OVERRIDED METHODS
    open override func draw(_ rect: CGRect) {
        if isTouching == true && isRecordingModeOn == false {
            let context = UIGraphicsGetCurrentContext()
            context?.saveGState()
            UIColor.white.setFill()
            UIColor.gray.setStroke()
            
            let midPoint = CGPoint(x: bounds.halfWidth, y: bounds.halfHeight)
            let circlePath = UIBezierPath(arcCenter: midPoint, radius: bounds.halfWidth - 16, startAngle: 0, endAngle: (3.14 * 2), clockwise: true)
            circlePath.lineWidth = 2
            circlePath.lineCapStyle = .round
            circlePath.lineJoinStyle = .bevel
            circlePath.close()
            circlePath.stroke()
            circlePath.fill()
            
            context?.restoreGState()
        }
    }
    
    open override func awakeFromNib() {
        initialize()
    }

    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        isTouching = true
        setNeedsDisplay()
        perform(#selector(self.videoRecordingStarted), with: nil, afterDelay: 1)
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isRecordingModeOn {
            videoRecordingStoped()
        }else {
            takePhoto()
            resetAll()
        }
        isTouching = false
        setNeedsDisplay()
    }
    
    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        if isRecordingModeOn {
            videoRecordingStoped()
        }else {
            takePhoto()
            resetAll()
        }
        isTouching = false
        setNeedsDisplay()
    }
    
    // MARK: - METHODS
    fileprivate func bounceEffectAnimation() {
        switch animationData.state {
        case .start:
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.animatedRecordingView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            }, completion: { (result) in
                if result {
                    self.animationData.state = .begin
                    self.bounceEffectAnimation()
                }
            })
            break
        case .begin:
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.animatedRecordingView.transform = CGAffineTransform(scaleX: 2, y: 2)
            }, completion: { (result) in
                if result {
                    self.animationData.state = .end
                    self.bounceEffectAnimation()
                }
            })
            break
        case .end:
            UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.animatedRecordingView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            }, completion: { (result) in
                if result {
                    self.animationData.state = .begin
                    self.bounceEffectAnimation()
                }
            })
            break
        }
    }
    
    fileprivate func resetAnimation() {
        animatedRecordingView.alpha = 0.0
        recordingView.alpha = 0.0
        CATransaction.begin()
            animatedRecordingView.layer.removeAllAnimations()
        CATransaction.commit()
        animatedRecordingView.bounds = animationData.targetViewBounds
        animatedRecordingView.layer.cornerRadius = animatedRecordingView.bounds.halfWidth
        animationData.state = .start
    }
    
    // MARK: - DELEGATES
    
    // MARK: - ACTIONS
    @objc fileprivate func videoRecordingStarted() {
        resetAll()
        isRecordingModeOn = true
        animatedRecordingView.alpha = 0.5
        recordingView.alpha = 1.0
        bounceEffectAnimation()
        
        if let delegate = delegate {
            delegate.cameraView(videoRecordStarted: self)
        }
    }
    
    fileprivate func videoRecordingStoped() {
        isRecordingModeOn = false
        resetAll()
        if let delegate = delegate {
            delegate.cameraView(videoRecordStopped: self)
        }
    }
    
    fileprivate func takePhoto() {
        if let delegate = delegate {
            delegate.cameraView(takePhoto: self)
        }
    }
    fileprivate func resetAll() {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        resetAnimation()
    }
    
    // MARK: - HELPERS
}
