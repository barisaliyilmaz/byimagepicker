//
//  BYImagePickerVideoPreview.swift
//  Pods
//
//  Created by Baris Yilmaz on 13/12/2016.
//
//

import UIKit
import AVFoundation
import AVKit

class BYImagePickerVideoPreview: UIViewController {
    // MARK: ******************************************
    // MARK: ************* PROPERTIES *****************
    // MARK: ******************************************
    // MARK: - OUTSIDE PROPERTIES
    var delegate:BYImagePickerViewDelegate?
    var videoURL:URL! = nil
    var videoOrientation:AVCaptureVideoOrientation! = nil
    
    // MARK: - LOCAL PROPERTIES
    var player:AVPlayer! = nil
    var playerItem:AVPlayerItem! = nil
    var playerLayer:AVPlayerLayer! = nil
    var assetURL:AVURLAsset! = nil
    var playerQueue = DispatchQueue(label: "com.chaos.byimagepicker-viewopreview-SessionQueue")
    
    var chaseTime:CMTime = kCMTimeZero
    var isPlaying:Bool = false
    var isSeekInProgress:Bool = false
    
    // MARK: - WIDGETS
    @IBOutlet weak var closeB: UIButton!
    @IBOutlet weak var sendB: UIButton!
    @IBOutlet weak var playB: UIButton!
    @IBOutlet weak var textView: BYMessageTextView!
    @IBOutlet weak var videoPlayer: UIView!
    @IBOutlet weak var videoRangeSlider: BYImagePickerVideoRangeSlider!
    
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    // MARK: ******************************************
    // MARK: ************** METHODS *******************
    // MARK: ******************************************
    // MARK: - INITIALIZATION
    deinit {
        playerItem.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), context: &playerItemContext)
    }
    
    fileprivate func initialize() {
        setupWidgets()
        
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        videoRangeSlider.addAction(target: self, selector: #selector(self.sliderValueChange_Action(_:)))
        setupPlayer()
    }
    
    var playerItemContext:Int = 1
    fileprivate func setupPlayer() {
        let options = [AVURLAssetPreferPreciseDurationAndTimingKey: true]
        assetURL    = AVURLAsset(url: videoURL, options: options)
        playerItem  = AVPlayerItem(asset: assetURL, automaticallyLoadedAssetKeys: ["playable", "duration", "exportable", "providesPreciseDurationAndTiming"])
        player      = AVPlayer(playerItem: playerItem)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame        = videoPlayer.frame
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspect
        videoPlayer.layer.addSublayer(playerLayer)

        createSequenceOfImages(assetURL: assetURL)
        playerItem.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new], context: &playerItemContext)
    }
    
    fileprivate func createSequenceOfImages(assetURL:AVURLAsset) {
        let keys = ["duration"]
        assetURL.loadValuesAsynchronously(forKeys: keys) { [unowned self] in
            var error:NSError? = nil
            let status = assetURL.statusOfValue(forKey: "duration", error: &error)
            
            switch status {
            case .loaded:
                if assetURL.tracks(withMediaType: AVMediaTypeVideo).count > 0 {
                    var valueList = [NSValue]()
                    let durationInSec = assetURL.duration.seconds
                    let itemCount = self.videoRangeSlider.itemCount
                    let tenthOfDuraction = durationInSec / Double(itemCount)
                    let imageGenerator = AVAssetImageGenerator(asset: assetURL)
                    
                    for index in 0..<itemCount {
                        let index = (index <= 0) ? 0.1 : Double(index)
                        let seconds = tenthOfDuraction * index
                        let time = CMTime(seconds: seconds, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
                        valueList.append(time as NSValue)
                    }
                    
                    var images = [UIImage]()
                    imageGenerator.generateCGImagesAsynchronously(forTimes: valueList) { (time, image, nextTime, result, error) in
                        switch result {
                        case .succeeded:
                            var imageOrientation:UIImageOrientation = .up
                            switch self.videoOrientation! {
                            case .portrait:
                                imageOrientation = .right
                            case .portraitUpsideDown:
                                imageOrientation = .left
                            case .landscapeLeft:
                                imageOrientation = .down
                            case .landscapeRight:
                                imageOrientation = .up
                            }
                            var img = UIImage(cgImage: image!, scale: 1.0, orientation: imageOrientation)
                            images.append(img)
                            if valueList.last! as CMTime == time {
                                DispatchQueue.main.async {
                                    self.videoRangeSlider.imageSequence = images
                                }
                            }
                        case .failed:
                            break
                        case .cancelled:
                            break
                        }
                    }
                }
                break
            case .loading:
                print("loading.")
                break
            case .failed:
                print("failed.")
                break
            case .cancelled:
                print("cancelled.")
                break
            default:
                break
            }
        }
    }
    
    fileprivate func setupWidgets() {
        let closeImage  = BYImagePickerUtils.imageFromBundle(imageName: "close")
        let sendImage   = BYImagePickerUtils.imageFromBundle(imageName: "send")
        let playImage   = BYImagePickerUtils.imageFromBundle(imageName: "play")
        
        closeB.setImage(closeImage, for: .normal)
        sendB.setImage(sendImage, for: .normal)
        playB.setImage(playImage, for: .normal)
        
        textView.layer.cornerRadius = textView.bounds.halfHeight
    }
    
    // MARK: - OVERRIDED METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        addObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        removeObservers()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        var theFrame = playerLayer.frame
        theFrame.size = size
        playerLayer.frame = theFrame
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &playerItemContext {
            if let path = keyPath, path == #keyPath(AVPlayerItem.status) {
                if let value = change?[.newKey] as? NSNumber {
                    let status = AVPlayerItemStatus(rawValue: value.intValue)!
                    switch status {
                    case .unknown:
                        print("unknown.")
                        
                    case .readyToPlay:
                        print("readyToPlay.")
                        addTimeObserver()
                        
                    case .failed:
                        print("failed.")
                       
                    }
                }else {
                    print("No value for path: \(path)")
                }
                
            }
        } else {
            return observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    // MARK: - METHODS
    fileprivate func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidPlayToEndTime(notification:)), name: Notification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
    
    fileprivate func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func addTimeObserver() {
        let timeInterval = CMTime(value: 1000, timescale: CMTimeScale(NSEC_PER_SEC))
        let duration = playerItem.duration
        player.addPeriodicTimeObserver(forInterval: timeInterval, queue: self.playerQueue) { [unowned self] (time) in
            let convertedDuration = CMTimeConvertScale(duration, time.timescale, CMTimeRoundingMethod.roundAwayFromZero)
            let sec = time.seconds
            let totalSec = convertedDuration.seconds
            let unitPosition = sec / totalSec
            DispatchQueue.main.async {
                self.videoRangeSlider.unitPos = CGFloat(unitPosition)
            }
        }
    }
    
    // MARK: - DELEGATES
    
    // MARK: - ACTIONS
    @IBAction func close_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func play_Action(_ sender: UIButton) {
        hidePlayButton(isHidden: true)
        player.play()
        isPlaying = true
    }
    
    @IBAction func send_Action(_ sender: UIButton) {
        var thumbnailImage = videoRangeSlider.imageSequence.first!
        var videoData:Data! = nil
        var caption:String = textView.text
        
        if assetURL.orientationForTrack() == UIInterfaceOrientation.portrait {
            assetURL.rotateVideo(completion: { (composition, videoComposition) in
                assetURL.exportVideo(asset: composition, videoComposition: videoComposition, completion: { (newVideoURL) in
                    do {
                        videoData = try Data(contentsOf: newVideoURL)
                        let oldVideoData = try Data(contentsOf: self.videoURL)
                        print("videoData.size = \(videoData.count/(1024)) KB - oldVideoVideoData.size = \(oldVideoData.count/(1024*1024)) MB")
                    } catch let error {
                        print("Cannot get video data from url :\(newVideoURL) with error \(error)")
                    }
                    if let delegate = self.delegate {
                        let vc = self.navigationController?.childViewControllers.first as! BYImagePickerViewController
                        delegate.imagePicker(imagePicker: vc, withVideoData: videoData, withThumbnail: thumbnailImage, withCaption: caption)
                    }
                    DispatchQueue.main.async { [unowned self] in
                        self.dismiss(animated: false, completion: nil)
                    }
                })
                
            })
        }else {
            let cacheDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first!
            var cacheURL = URL(fileURLWithPath: cacheDirectory)
            cacheURL.appendPathComponent("temp-video.mp4")
            
            if FileManager.default.fileExists(atPath: cacheURL.path) {
                do {
                    try FileManager.default.removeItem(at: cacheURL)
                }catch let error {
                    debugPrint(error)
                }
            }
            
            let exportSession = AVAssetExportSession(asset: assetURL, presetName: AVAssetExportPreset640x480)
            exportSession!.outputURL = cacheURL
            exportSession!.outputFileType = AVFileTypeQuickTimeMovie
            exportSession!.exportAsynchronously {
                switch exportSession!.status {
                case .completed:
                    do {
                        videoData = try Data(contentsOf: exportSession!.outputURL!)
                        let oldVideoData = try Data(contentsOf: self.videoURL)
                        print("videoData.size = \(videoData.count/1024) KB - oldVideoVideoData.size = \(oldVideoData.count/1024) KB")
                    } catch let error {
                        print("Cannot get video data from url :\(exportSession!.outputURL!) with error \(error)")
                    }
                    if let delegate = self.delegate {
                        let vc = self.navigationController?.childViewControllers.first as! BYImagePickerViewController
                        delegate.imagePicker(imagePicker: vc, withVideoData: videoData, withThumbnail: thumbnailImage, withCaption: caption)
                    }
                    self.dismiss(animated: false, completion: nil)
                    
                case .failed:
                    break
                default:
                    break
                }
            }
            
        }
    }
    
    @objc fileprivate func playerItemDidPlayToEndTime(notification:Notification) {
        let startTime = kCMTimeZero// CMTime(value: 0, timescale: CMTimeScale(NSEC_PER_SEC))
        player.seek(to: startTime)
        hidePlayButton(isHidden: false)
    }
    
    @IBAction func tap_Action(_ sender: UITapGestureRecognizer) {
        if isPlaying {
            hidePlayButton(isHidden: false)
            player.pause()
            isPlaying = !isPlaying
        }
    }
    @IBAction func sliderValueChange_Action(_ sender: BYImagePickerVideoRangeSlider) {
        let duration                    = playerItem.duration
        let newDuration                 = CMTimeConvertScale(duration, CMTimeScale(NSEC_PER_SEC), .roundAwayFromZero)
        
        let timeValue                   = newDuration.value
        let timeValueF                  = Float(timeValue)
        let currentValue:CMTimeValue    = CMTimeValue( timeValueF  * Float(sender.unitPos))
        let chaseTime:CMTime            = CMTime(value: currentValue, timescale: newDuration.timescale)
        
        if self.chaseTime != chaseTime {
            self.chaseTime = chaseTime
            if isSeekInProgress == false {
                isSeekInProgress = true
                let seekInProgress = self.chaseTime
                player.seek(to: chaseTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero) { [unowned self] (isGranted) in
                    self.isSeekInProgress = isGranted ? false : true
                }
            }
        }else {
            print("Same chaseTime: \(chaseTime). Do not operate seek function.")
        }
    }
    // MARK: - HELPERS
    fileprivate func hidePlayButton(isHidden:Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseInOut, animations: {
                self.playB.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
                self.playB.alpha = 0.0
            }, completion: nil)
        }else {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseInOut, animations: {
                self.playB.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.playB.alpha = 1.0
            }, completion: nil)
        }
    }
}

// MARK: - Keyboard Operations
extension BYImagePickerVideoPreview {
    open func keyboardWillShow(notification: NSNotification) {
        let frame:CGRect = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let duration:Double = (notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let animationCurve = (notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uintValue
        let animationOption = UIViewAnimationOptions(rawValue: animationCurve)
        
        UIView.animate(withDuration: duration, delay: 0.0, options: animationOption, animations: {
            self.textViewBottomConstraint.constant += frame.height
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    open func keyboardWillHide(notification: NSNotification) {
        let frame:CGRect = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let duration:Double = (notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let animationCurve = (notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uintValue
        let animationOption = UIViewAnimationOptions(rawValue: animationCurve)
        
        UIView.animate(withDuration: duration, delay: 0.0, options: animationOption, animations: {
            self.textViewBottomConstraint.constant -= frame.height
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
