//
//  BYImagePickerPhotoPreview.swift
//  Pods
//
//  Created by Baris Yilmaz on 13/12/2016.
//
//

import UIKit

class BYImagePickerPhotoPreview: UIViewController {

    // MARK: ******************************************
    // MARK: ************* PROPERTIES *****************
    // MARK: ******************************************
    // MARK: - OUTSIDE PROPERTIES
    var delegate:BYImagePickerViewDelegate?
    var image:UIImage! = nil
    
    // MARK: - LOCAL PROPERTIES
    
    // MARK: - WIDGETS
    @IBOutlet weak var pictureIV: UIImageView!
    @IBOutlet weak var closeB: UIButton!
    @IBOutlet weak var messageTV: BYMessageTextView!
    @IBOutlet weak var sendB: UIButton!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    
    // MARK: ******************************************
    // MARK: ************** METHODS *******************
    // MARK: ******************************************
    // MARK: - INITIALIZATION
    fileprivate func initialize() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
        
        setupWidgets()
    }
    
    fileprivate func setupWidgets() {
        let closeImage  = BYImagePickerUtils.imageFromBundle(imageName: "close")
        let sendImage   = BYImagePickerUtils.imageFromBundle(imageName: "send")
        let playImage   = BYImagePickerUtils.imageFromBundle(imageName: "play")
        
        closeB.setImage(closeImage, for: .normal)
        sendB.setImage(sendImage, for: .normal)
        pictureIV.image = self.image
        
        messageTV.layer.cornerRadius = messageTV.bounds.halfHeight
    }
    
    // MARK: - OVERRIDED METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    // MARK: - METHODS
    
    // MARK: - DELEGATES
    
    // MARK: - ACTIONS
    @IBAction func close_Action(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func send_Action(_ sender: UIButton) {
        let image = self.image
        let text = messageTV.text
        if let delegate = delegate {
            let vc = navigationController?.childViewControllers.first as! BYImagePickerViewController
            delegate.imagePicker(imagePicker: vc, withImage: image!, withCaption: text!)
        }
        
        dismiss(animated: true, completion: nil)
    }
    // MARK: - HELPERS

}

// MARK: - Keyboard Operations
extension BYImagePickerPhotoPreview {
    open func keyboardWillShow(notification: NSNotification) {
        let frame:CGRect = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let duration:Double = (notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let animationCurve = (notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uintValue
        let animationOption = UIViewAnimationOptions(rawValue: animationCurve)
        
        UIView.animate(withDuration: duration, delay: 0.0, options: animationOption, animations: {
            self.textViewBottomConstraint.constant += frame.height
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    open func keyboardWillHide(notification: NSNotification) {
        let frame:CGRect = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let duration:Double = (notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let animationCurve = (notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! NSNumber).uintValue
        let animationOption = UIViewAnimationOptions(rawValue: animationCurve)
        
        UIView.animate(withDuration: duration, delay: 0.0, options: animationOption, animations: {
            self.textViewBottomConstraint.constant -= frame.height
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
