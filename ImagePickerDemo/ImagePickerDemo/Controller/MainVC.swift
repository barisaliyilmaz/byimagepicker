//
//  MainVC.swift
//  ImagePickerDemo
//
//  Created by Baris Yilmaz on 08/12/2016.
//  Copyright © 2016 Baris Yilmaz. All rights reserved.
//

import UIKit
import BYImagePicker

class MainVC: UIViewController {

    @IBOutlet weak var photoPreviewIV: UIImageView!
    @IBOutlet weak var videoPreviewIV: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func navigateToImagePicker_Action(_ sender: UIButton) {
        let vc:BYImagePickerViewController = storyboard?.instantiateViewController(withIdentifier: "BYImagePickerViewController") as! BYImagePickerViewController
        vc.delegate = self
        let navController = UINavigationController(rootViewController: vc)
        navController.isNavigationBarHidden = true
        
        present(navController, animated: true, completion: nil)
    }
}

extension MainVC: BYImagePickerViewDelegate {
    func imagePicker(imagePicker: BYImagePickerViewController, withImage image: UIImage, withCaption text: String?) {
        photoPreviewIV.image = image
        let imageData = UIImageJPEGRepresentation(image, 1)
        print("image.size :\((imageData?.count)! / (1024)) KB \((imageData?.count)! / (1024*1042)) MB")
    }
    
    func imagePicker(imagePicker: BYImagePickerViewController, withVideoData data: Data, withThumbnail image: UIImage, withCaption text: String?) {
        videoPreviewIV.image = image
        print("data.size :\(data.count/1024) KB.")
    }
}
